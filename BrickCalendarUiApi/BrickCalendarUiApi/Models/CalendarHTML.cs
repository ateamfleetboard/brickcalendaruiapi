﻿using System;
using System.Collections.Generic;

namespace BrickCalendarUiApi.Models
{
    public class CalendarHTML
    {
        public CalendarHTML()
        {
            Months = new List<Month>();
        }

        public List<Month> Months { get; set; }

        public string IsWiki { get; set; }
    }

    public class Day
    {
        public Day()
        {
            Appointments = new List<Appointment>();
        }

        public DateTime Date { get; set; }
        public List<Appointment> Appointments { get; set; }

        public string InString => Date.ToString("dd");

        public int InSimpleInt => Date.Day;

        public string Name => Date.ToString("dddd");

        public string ShortName => Date.ToString("ddd");

        public bool IsToday => Date.ToString("yy-MM-dd") == DateTime.Now.ToString("yy-MM-dd");

        public string TodayCss => IsToday ? "today" : "";

        public bool IsFromPreviousMonth { get; set; }

        public string IsFromPreviousMonthCss
            => IsFromPreviousMonth ? "calendar-entry-container-pre" : "calendar-entry-container";

        public bool IsWeekend => (int) Date.DayOfWeek == 6 || (int) Date.DayOfWeek == 0;

        public string WeekendCss => IsWeekend ? "weekend" : "";
    }

    public class Month
    {
        public Month(DateTime oneDayOfMonth)
        {
            monthInformation = oneDayOfMonth;
            Days = new List<Day>();
        }

        private DateTime monthInformation;

        public List<Day> Days { get; set; }

        public string Name => monthInformation.ToString("MMMM");

        public int MonthNumber => Convert.ToInt32(monthInformation.ToString("MM"));

        public int Year => monthInformation.Year;

        public int DaysInMonth => DateTime.DaysInMonth(Year, MonthNumber);

        public int DaysOfPreviousMonth { get; set; }

        public int DaysInMonthWithPrevious => DaysInMonth + DaysOfPreviousMonth;
    }

    public class Appointment
    {
        public string Employee { get; set; }
        public string Detail { get; set; }
        public string Type { get; set; }

        public string DetailShortCut
        {
            get
            {
                string returnValue;

                switch (Detail.ToLower())
                {
                    case "red":
                        returnValue = "u";
                        break;
                    case "yellow":
                        returnValue = "g";
                        break;
                    case "green":
                        returnValue = "h";
                        break;
                    case "blue":
                        returnValue = "s";
                        break;
                    default:
                        returnValue = string.Empty;
                        break;
                }

                return returnValue;
            }
        }
    }
}