﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BrickCalendarUiApi.Models
{
    public class CalendarJSON
    {
        [JsonProperty("_embedded")]
        public EmbeddedJSON EmbeddedJson { get; set; }
    }

    public class EmbeddedJSON
    {
        [JsonProperty("calendarItemList")]
        public List<CalendarItemListJSON> CalendarItemListJson { get; set; }
    }

    public class CalendarItemListJSON
    {
        [JsonProperty("date")]
        public DateTime DateJson { get; set; }

        [JsonProperty("appointments")]
        public List<AppointmentsJson> AppointmentsJson { get; set; }
    }

    public class AppointmentsJson
    {
        [JsonProperty("employee")]
        public string EmployeeJson { get; set; }

        [JsonProperty("detail")]
        public string DetailJson { get; set; }

        [JsonProperty("type")]
        public string TypeJson { get; set; }
    }
}