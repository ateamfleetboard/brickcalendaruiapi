﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using BrickCalendarUiApi.Models;
using Newtonsoft.Json;

namespace BrickCalendarUiApi.Controllers
{
    public class HtmlController : Controller
    {
        //GET: Html
        public async Task<ActionResult> Index(string wiki)
        {
            var url = ConfigurationManager.AppSettings["BrickCalendarBackendService"];
            var client = new HttpClient {BaseAddress = new Uri(url)};

            client.Timeout = TimeSpan.FromSeconds(10);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                var responseMessage = await client.GetAsync(url);

                if (!responseMessage.IsSuccessStatusCode) return View("Error");

                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                var calendarJson = JsonConvert.DeserializeObject<CalendarJSON>(responseData);
                var calendarHtml = ConvertJsonToHtml(calendarJson);

                FillUpMissingCalendarDays(calendarHtml);
                OrderCalendarDays(calendarHtml);

                calendarHtml.IsWiki = wiki;

                return View(calendarHtml);
            }
            catch
            {
                return View("Error");
            }
        }

        public ActionResult Error()
        {
            return View();
        }

        private static CalendarHTML ConvertJsonToHtml(CalendarJSON calendarJson)
        {
            var calendarHtml = new CalendarHTML();
            var calendarEntries = calendarJson.EmbeddedJson.CalendarItemListJson;
            var monthsOfJson = calendarEntries.GroupBy(x => x.DateJson.Month).ToList();

            foreach (var monthOfJson in monthsOfJson)
            {
                var month = new Month(monthOfJson.First().DateJson);

                foreach (var dayOfMonth in monthOfJson)
                {
                    var day = new Day {Date = dayOfMonth.DateJson};

                    foreach (var appointment in dayOfMonth.AppointmentsJson)
                    {
                        day.Appointments.Add(new Appointment
                        {
                            Detail = appointment.DetailJson,
                            Employee = appointment.EmployeeJson,
                            Type = appointment.TypeJson
                        });
                    }

                    month.Days.Add(day);
                }

                calendarHtml.Months.Add(month);
            }

            return calendarHtml;
        }

        private static void FillUpMissingCalendarDays(CalendarHTML calendarHtml)
        {
            foreach (var month in calendarHtml.Months)
            {
                for (var day = 1; day <= month.DaysInMonth; day++)
                {
                    var dayWithoutAppointments = month.Days.Find(y => y.Date.Day == day);

                    if (dayWithoutAppointments == null)
                    {
                        month.Days.Add(new Day
                        {
                            Date = new DateTime(month.Year, month.MonthNumber, day)
                        });
                    }
                }
            }
        }

        private static void OrderCalendarDays(CalendarHTML calendarHtml)
        {
            foreach (var month in calendarHtml.Months)
            {
                var day = month.Days.OrderBy(x => x.Date.Day).First();
                var firstDayOfMonth = (int) day.Date.DayOfWeek;
                var actDate = new DateTime(month.Year, month.MonthNumber, 1);
                var preDate = actDate.AddMonths(-1);

                var numberOfDaysFromPreviousMonth = DateTime.DaysInMonth(preDate.Year, preDate.Month);
                var missingDays = new List<Day>();

                if (firstDayOfMonth == 0)
                {
                    firstDayOfMonth = 7;
                }

                if (firstDayOfMonth != 1)
                {
                    for (var z = 0; z < firstDayOfMonth - 1; z++)
                    {
                        month.DaysOfPreviousMonth++;
                        var missingDay = new Day
                        {
                            Date = new DateTime(preDate.Year, preDate.Month, numberOfDaysFromPreviousMonth - z),
                            IsFromPreviousMonth = true
                        };

                        AddAppointmentsIfExist(missingDay, calendarHtml.Months);

                        missingDays.Add(missingDay);
                    }
                }


                month.Days.AddRange(missingDays);
                month.Days = month.Days.OrderBy(x => x.Date).ToList();
            }
        }

        private static void AddAppointmentsIfExist(Day missingDay, IEnumerable<Month> months)
        {
            var monthOfMissingDay =
                months.FirstOrDefault(x => x.Year == missingDay.Date.Year && x.MonthNumber == missingDay.Date.Month);

            var originalDay =
                monthOfMissingDay?.Days.FirstOrDefault(
                    d => d.InSimpleInt == missingDay.InSimpleInt && !d.IsFromPreviousMonth);

            if (originalDay != null)
            {
                missingDay.Appointments.AddRange(originalDay.Appointments);
            }
        }

        //public ActionResult Index()
        //{
        //    var calendarHTML = DoWhatYouWant();

        //    return View(calendarHTML);
        //}

        //private CalendarHTML DoWhatYouWant()
        //{
        //    var json = System.IO.File.ReadAllText(@"C:\calendarJsonFormat.json");
        //    var calendarJson = JsonConvert.DeserializeObject<CalendarJSON>(json);
        //    var calendarHtml = ConvertJsonToHtml(calendarJson);

        //    FillUpMissingCalendarDays(calendarHtml);
        //    OrderCalendarDays(calendarHtml);

        //    return calendarHtml;
        //}
    }
}